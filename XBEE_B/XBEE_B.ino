int inPin = 7;
int toggle = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(inPin, INPUT);
  Serial.write("XBEE_B Ready !");
}

void onDetectON(){
   Serial.write("Value is HIGH");
}

void onDetectOFF(){
   Serial.write("Value is LOW");
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(inPin)==HIGH && toggle==0){
    onDetectON();
    toggle = 1;
  }
  if(digitalRead(inPin)==LOW && toggle==1){
    onDetectOFF();
    toggle = 0;
  }
  delay(1000);
  }
