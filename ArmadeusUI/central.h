#ifndef CENTRAL_H
#define CENTRAL_H

#define BAUDRATE B9600
#define XSTICK "/dev/ttyUSB0"

#define FALSE 0
#define TRUE 1
#define CMD_SCAN 1
#define CMD_FCT 2
#define CMD_UNI 3
#define CMD_BROAD 4
#define CMD_READ 5


struct node {
    char *nodeName; //
    char *nodeDesc; // Description des capteurs de la node
};

void signal_handler_IO(int status);   /* Appelé lors de la réception d'un signal SIGIO (écriture sur le device) */

void processIO(char buf[255], int res);

typedef void (*callback)(struct node);

int setUpConnection(callback ptr_reg_callback);

void send_cmd(int index,int cmd);

#endif
