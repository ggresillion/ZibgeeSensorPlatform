#pragma once

#include <QWidget>
#include <qpushbutton.h>
#include <QObject>
#include <qtablewidget.h>
#include "mythread.h"

class MainWindow : public QWidget {

    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    MyThread *mThread;
    QTableWidget *tableWidget;
private slots:
    void startScan();
    void quitApp();
    void setBroadcastMode();
    void setUnicastMode();
    void read();
    void doAction();
public slots:
    void onDicoveredNewDevice(QString nodeName, QString nodeDesc);
private:
    QPushButton *scanBtn;
    QPushButton *quitBtn;
    QPushButton *broadcastBtn;
    QPushButton *unicastBtn;
    QPushButton *actionBtn;
    QPushButton *readBtn;
    int deviceCount=0;
    QItemSelectionModel *select;
};
