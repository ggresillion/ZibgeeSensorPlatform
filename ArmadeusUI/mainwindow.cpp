#include <QFuture>
#include <QGridLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QTimer>
#include <qapplication.h>
#include <qheaderview.h>
#include "mainwindow.h"
#include "devices.h"
extern "C" {
    #include "central.h"
}

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent) {

  //LAYOUT
  QGridLayout *grid = new QGridLayout(this);

  //TABLE
  tableWidget = new QTableWidget(this);
  tableWidget->setRowCount(10);
  tableWidget->setColumnCount(2);
  QHeaderView *verticalHeader = tableWidget->verticalHeader();
  verticalHeader->setSectionResizeMode(QHeaderView::Stretch);
  verticalHeader->setStretchLastSection(true);
  QStringList m_TableHeader;
  m_TableHeader<<"Name"<<"Description";
  tableWidget->setHorizontalHeaderLabels(m_TableHeader);
  QHeaderView *header =  tableWidget->horizontalHeader();
  header->setStretchLastSection(true);
  header->setSectionResizeMode(QHeaderView::Stretch);
  QFont font;
  font.setPointSize(20);
  tableWidget->setFont(font);
  select = tableWidget->selectionModel();

  //SCAN BUTTON
  scanBtn = new QPushButton("Scan all nodes",this);
  connect(scanBtn, SIGNAL (clicked()),this, SLOT (startScan()));
  scanBtn->setMinimumHeight(100);

  //UNICAST BUTTON
  unicastBtn = new QPushButton("Unicast mode",this);
  connect(scanBtn, SIGNAL (clicked()),this, SLOT (setUnicastMode()));
  unicastBtn->setMinimumHeight(100);

  //BROADCAST BUTTON
  broadcastBtn = new QPushButton("Broadcast mode",this);
  connect(scanBtn, SIGNAL (clicked()),this, SLOT (setBroadcastMode()));
  broadcastBtn->setMinimumHeight(100);

  //READ BUTTON
  readBtn = new QPushButton("Read",this);
  connect(readBtn, SIGNAL (clicked()),this, SLOT (read()));
  readBtn->setMinimumHeight(100);

  //ACTION BUTTON
  actionBtn = new QPushButton("Action",this);
  connect(actionBtn, SIGNAL (clicked()),this, SLOT (doAction()));
  actionBtn->setMinimumHeight(100);

  //QUIT BUTTON
  quitBtn = new QPushButton("Quit",this);
  connect(quitBtn, SIGNAL (clicked()),this, SLOT (quitApp()));
  quitBtn->setMinimumHeight(100);

  //ADD WIDGETS
  grid->addWidget(scanBtn, 0, 1, 1, 1);
  grid->addWidget(quitBtn, 19, 1, 1, 1);
  grid->addWidget(unicastBtn, 1, 1, 1, 1);
  grid->addWidget(broadcastBtn, 2, 1, 1, 1);
  grid->addWidget(readBtn, 3, 1, 1, 1);
  grid->addWidget(actionBtn, 4, 1, 1, 1);
  grid->addWidget(tableWidget,0,0,20, 1);


  //FULLSCREEN
  QTimer::singleShot(1000, this, SLOT(showFullScreen()));
  setLayout(grid);

  //START THREAD
  mThread = new MyThread(this);
  connect(mThread, SIGNAL(dicoveredNewDevice(QString, QString)),
              this, SLOT(onDicoveredNewDevice(QString, QString)));
  mThread->start();
}

void MainWindow::startScan(){
    //tableWidget->setRowCount(0);
}

void MainWindow::quitApp(){
    QApplication::quit();
}

void MainWindow::onDicoveredNewDevice(QString nodeName, QString nodeDesc)
{
    printf("Main received data");
    tableWidget->setItem(deviceCount,0, new QTableWidgetItem(nodeName));
    tableWidget->setItem(deviceCount,1, new QTableWidgetItem(nodeDesc));
    deviceCount++;
}


void MainWindow::setUnicastMode(){
    int row = select->currentIndex().row();
    send_cmd(row,CMD_UNI);
}

void MainWindow::setBroadcastMode(){
    int row = select->currentIndex().row();
    send_cmd(row,CMD_UNI);
}

void MainWindow::read(){
    send_cmd(0,CMD_READ);
}

void MainWindow::doAction(){
    send_cmd(0,CMD_FCT);
}
