#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <stdlib.h>
#include <memory.h>
#include <time.h>
#include "central.h"
#include <mxml.h>

volatile int STOP = FALSE;
struct node node_array[2];
int node_count;

int fd_usb;
char *destNode;
callback onIO;

int wait_flag = TRUE;                /* TRUE quand aucun signal reçu */

int setUpConnection(callback ptr_reg_callback) {
    onIO = ptr_reg_callback;
    int c, res;
    struct termios oldtio, newtio;
    struct sigaction saio;           /* definition du signal action */
    char buf[255];

    /* ouverture du device en mode non bloquant. */
    fd_usb = open(XSTICK, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd_usb < 0) {
        perror(XSTICK);
        exit(-1);
    }

    /* attribue le signal handler */
    saio.sa_handler = signal_handler_IO;
    sigemptyset(&(saio.sa_mask));
    saio.sa_flags = 0;
    saio.sa_restorer = NULL;
    sigaction(SIGIO, &saio, NULL);

    /* permet au processus de recevoir SIGIO */
    fcntl(fd_usb, F_SETOWN, getpid());

    fcntl(fd_usb, F_SETFL, FASYNC);

    tcgetattr(fd_usb, &oldtio); /* save current port settings */
    /* set new port settings for canonical input processing */
    newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR | ICRNL;
    newtio.c_oflag = 0;
    newtio.c_lflag = ICANON;
    newtio.c_cc[VMIN] = 1;
    newtio.c_cc[VTIME] = 0;
    tcflush(fd_usb, TCIFLUSH);
    tcsetattr(fd_usb, TCSANOW, &newtio);

    send_cmd(1,CMD_SCAN);

    /* boucle en attente d'une entrée */
    while (STOP == FALSE) {
        sleep(1);
        /* après réception du SIGIO, wait_flag = false, on peut donc
         * lire l'entrée */
        if (wait_flag == FALSE) {
            res = read(fd_usb, buf, 255);
            buf[res] = 0;
            printf("%s -> %d bytes read.\n", buf, res - 1);
            processIO(buf, res);
            wait_flag = TRUE;      /* wait for new input */
        }
        //xml_setup();

    }
    /* restore old port settings */
    tcsetattr(fd_usb, TCSANOW, &oldtio);
}

/**
 * Récupère le temps pour le log des données
 *
 * @return tm
 */
struct tm *getTime() {
    struct tm *res;
    time_t rawtime;
    time(&rawtime);
    res = localtime(&rawtime);

    return res;
}

/**
 * Processe les commandes lues
 *
 * @param buf   buffer récupéré depuis le port
 * @param size  nombre de caractères lus
 */
void processIO(char *buf, int size) {

    if (size > 19 && (strncmp(buf, "connect", size - 19)) == 0) {

        node_array[node_count].nodeName = malloc(sizeof(char) * 6);
        strncpy(node_array[node_count].nodeName, &buf[8], 6);

        node_array[node_count].nodeDesc = malloc(sizeof(char) * 10);
        strncpy(node_array[node_count].nodeDesc, &buf[15], 11);
        (*onIO)(node_array[node_count]);
        printf("Connection de : %s | Description : %s", node_array[node_count].nodeName,
               node_array[node_count].nodeDesc);
        node_count++;
    }

    else if (size > 7 && strncmp(buf, "payload", size - 8)) {
            char *payload;
            strcpy(payload, &buf[8]);
            printf(payload);
        }
        if (node_count == 2) {
            node_count = 0;
            printf("Premier node: %s", node_array[0].nodeDesc);
            printf("Second node: %s", node_array[1].nodeDesc);
        }
}

void send_cmd(int index, int cmd) {
    char pre_cmd[10] = "ATDN\0"; // taille suffisante pour éviter un buffer overrun

    switch (cmd) {
        case CMD_SCAN:
            write(fd_usb, "scan.", 5); // Envoie une requête à chaque node du réseau
            sleep(1);
            write(fd_usb, 13, 2);
            break;

        case CMD_FCT:
            write(fd_usb, "sensor_fct.", 11);
            break;

        case CMD_UNI:
            strcat(pre_cmd, "NODE1");
            strcat(pre_cmd, "\r");
            write(fd_usb, "+++", 3);
            sleep(1);
            write(fd_usb, pre_cmd, 10);
            break;

        case CMD_BROAD:
            write(fd_usb, "+++", 3);
            sleep(1);
            write(fd_usb, "ATDLFFFF\r", 9); // 16 bits de poids faible de l'addresse de broadcast
            sleep(1);
            write(fd_usb, "ATDH0\r", 6); // 16 bits de poids fort
            break;

        case CMD_READ:
            write(fd_usb, "read.", 5);
            sleep(1);
            break;
    }
}



/***************************************************************************
* signal handler. change la valeur de wait_flag pour signaler à la boucle  *
* que des charactères ont été écrits sur le device.                        *
***************************************************************************/

void signal_handler_IO(int status) {
    printf("received SIGIO signal.\n");
    wait_flag = FALSE;
}

void xml_setup() {
    FILE *fp;

    fp = fopen("data.xml", "w");

    mxml_node_t *xml;    /* <?xml_setup ... ?> */
    mxml_node_t *data;   /* <data> */
    mxml_node_t *node1;   /* <node> */
    mxml_node_t *node2;
    mxml_node_t *node_name;
    mxml_node_t *node_desc;

    mxml_node_t *timestamp; /* <timestamp> */
    mxml_node_t *sensor_data; /* <sensor_data> */
    mxml_node_t *payload; /* <payload> */

    xml = mxmlNewXML("1.0");

    data = mxmlNewElement(xml, "data");

    node1 = mxmlNewElement(data, "node1");
    node_name = mxmlNewElement(node1, "node_name");
    mxmlNewText(node_name, 0, node_array[0].nodeName);
    node_desc = mxmlNewElement(node1, "node_desc");
    mxmlNewText(node_desc, 0, node_array[0].nodeDesc);
    sensor_data = mxmlNewElement(node1, "sensor_data");
    timestamp = mxmlNewElement(sensor_data, "timestamp");
    payload = mxmlNewElement(sensor_data, "payload");

    char time[20];
    strftime(&time, 20, "%d/%m/%Y %H:%M:%S", getTime());
    mxmlNewText(payload, 0, "val1");
    mxmlNewText(timestamp, 0, time);

    node2 = mxmlNewElement(data, "node2");
    node_name = mxmlNewElement(node2, "node_name");
    mxmlNewText(node_name, 0, node_array[1].nodeName);
    node_desc = mxmlNewElement(node2, "node_desc");
    mxmlNewText(node_desc, 0, node_array[1].nodeDesc);
    sensor_data = mxmlNewElement(node2, "sensor_data");
    timestamp = mxmlNewElement(sensor_data, "timestamp");
    payload = mxmlNewElement(sensor_data, "payload");

    strftime(&time, 20, "%d/%m/%Y %H:%M:%S", getTime());
    mxmlNewText(payload, 0, "val1");
    mxmlNewText(timestamp, 0, time);


    mxmlSaveFile(xml, fp, MXML_NO_CALLBACK);
    fclose(fp);
}

void add_xml_data(int index, const char data) {
    FILE *fp;
    mxml_node_t *root;
    mxml_node_t *node;
    mxml_node_t *sensor_data;
    mxml_node_t *payload;
    mxml_node_t *timestamp;


    fp = fopen("data.xml", "rw");
    root = mxmlLoadFile(NULL, fp, MXML_TEXT_CALLBACK);

    switch (index) {
        case 1:
            node = mxmlFindElement(root, root, "node1", NULL, NULL, MXML_DESCEND);
            break;
        case 2:
            node = mxmlFindElement(root, root, "node2", NULL, NULL, MXML_DESCEND);
            break;
    }

    sensor_data = mxmlNewElement(node, "sensor_data");
    timestamp = mxmlNewElement(sensor_data, "timestamp");
    payload = mxmlNewElement(sensor_data, "sensor_data");

    char time[20];
    strftime(&time, 20, "%d/%m/%Y %H:%M:%S", getTime());
    mxmlNewText(timestamp, 0, time);
    mxmlNewText(payload, 0, "test §");

    mxmlSaveFile(root, fp, MXML_NO_CALLBACK);
    fclose(fp);


}
