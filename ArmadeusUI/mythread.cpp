#include "mythread.h"
#include <QDebug>
#include <QMutex>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <stdlib.h>
#include <memory.h>
extern "C" {
    #include "central.h"
}

MyThread *threadRef;

void onIO(struct node node){
    emit threadRef->dicoveredNewDevice(node.nodeName, node.nodeDesc);
}

MyThread::MyThread(QObject *parent, bool b) :
    QThread(parent), Stop(b)
{

}

// run() will be called when a thread starts
void MyThread::run()
{
    threadRef = this;
    setUpConnection(onIO);
}
