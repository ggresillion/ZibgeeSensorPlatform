include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <stdlib.h>
#include <memory.h>

#define BAUDRATE B9600
#define MODEMDEVICE "/dev/ttyUSB0"
#define FALSE 0
#define TRUE 1
#define CMD_SCAN "scan."
#define CMD_FCT "sensor_fct." // Commande à envoyer pour activer la LED/récuperer des données.


struct node {
    char *nodeName; // Nom de la node découverte
    char *nodeDesc; // Description des capteurs de la node
    int SH;
    int SL;
    int AI;
};


volatile int STOP = FALSE;
int fd_usb;

void signal_handler_IO(int status);   /* definition of signal handler */
void processIO(char buf[255], int res);

int wait_flag = TRUE;                    /* TRUE while no signal received */

main() {
    int c, res;
    struct termios oldtio, newtio;
    struct sigaction saio;           /* definition of signal action */
    char buf[255];

    /* open the device to be non-blocking (read will return immediately) */
    fd_usb = open(MODEMDEVICE, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd_usb < 0) {
        perror(MODEMDEVICE);
        exit(-1);
    }

    /* install the signal handler before making the device asynchronous */
    saio.sa_handler = signal_handler_IO;
    sigemptyset(&(saio.sa_mask));
    saio.sa_flags = 0;
    saio.sa_restorer = NULL;
    sigaction(SIGIO, &saio, NULL);

    /* allow the process to receive SIGIO */
    fcntl(fd_usb, F_SETOWN, getpid());
    /* Make the file descriptor asynchronous (the manual page says only
       O_APPEND and O_NONBLOCK, will work with F_SETFL...) */
    fcntl(fd_usb, F_SETFL, FASYNC);

    tcgetattr(fd_usb, &oldtio); /* save current port settings */
    /* set new port settings for canonical input processing */
    newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR | ICRNL;
    newtio.c_oflag = 0;
    newtio.c_lflag = ICANON;
    newtio.c_cc[VMIN] = 1;
    newtio.c_cc[VTIME] = 0;
    tcflush(fd_usb, TCIFLUSH);
    tcsetattr(fd_usb, TCSANOW, &newtio);

    sleep(1);
    //unicastMode();

    //write(fd_usb, CMD_SCAN, 5);
    write(fd_usb, CMD_FCT, 11);


    /* loop while waiting for input. normally we would do something
       useful here */
    while (STOP == FALSE) {
        usleep(100000);
        /* after receiving SIGIO, wait_flag = FALSE, input is available
           and can be read */
        if (wait_flag == FALSE) {
            res = read(fd_usb, buf, 255);
            buf[res] = 0;
            printf("%s -> %d bytes read.\n", buf, res - 1);
            processIO(buf, res);
            wait_flag = TRUE;      /* wait for new input */
        }
    }
    /* restore old port settings */
    tcsetattr(fd_usb, TCSANOW, &oldtio);
}

void processIO(char *buf, int size) {

    char nodeName[5];
    if (size > 10 && (strncmp(buf, "connect", size - 19)) == 0) {

        memcpy(nodeName, &buf[8], 6); // Copie du nom de la node
        struct node connectedNode;

        connectedNode.nodeName = malloc(sizeof(char) * 6);
        strncpy(connectedNode.nodeName, &buf[8], 6);
        connectedNode.nodeDesc = malloc(sizeof(char) * 10);
        strncpy(connectedNode.nodeDesc, &buf[15],10);

        printf("Connection de : %s (Description : %s).\n", connectedNode.nodeName, connectedNode.nodeDesc);

    }

}

void sendCmd(char *nodeName, char *CMD) {
    
}
/**
 * Sets the XBee cordinator in unicast mode.
 * @return
 */
int unicastMode(char *targetNode) {
    char buf[255];

    write(fd_usb, "+++", 3);
    sleep(1);
    read(fd_usb, buf, 255);
    printf("buffer read: %s", buf);
    sleep(3);

    write(fd_usb, "ATDN\n", 5);

    read(fd_usb, buf, 255);

    printf("buffer read: %s", buf);

}

/***************************************************************************
* signal handler. change la valeur de wait_flag pour signaler à la boucle  *
* que des charactères ont été écrits sur le device.                        *
***************************************************************************/

void signal_handler_IO(int status) {
    printf("received SIGIO signal.\n");
    wait_flag = FALSE;
}
    
