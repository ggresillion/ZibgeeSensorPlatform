
//ON 111 110
//OFF 111 102 102
//RETURN 13

int pin = 13;
int incomingByte = 0;
int buffer[3];
int count = 0;

void execCommand() {
  // ON
  if (buffer[0] == 111 && buffer[1] == 110) {
    Serial.write("Turning ON \r\n");
    digitalWrite(pin, HIGH);
  }
  // OFF
  else if (buffer[0] == 111 && buffer[1] == 102 && buffer[2] == 102) {
    Serial.write("Turning OFF \r\n");
    digitalWrite(pin, LOW);
  }
  else {
    Serial.write("Bad command \r\n");
  }
  Serial.write( '\r' );
  count = 0;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(pin, OUTPUT);
  Serial.write("XBEE_A Ready ! \r\n");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    if (incomingByte == 13) {
      execCommand();
    } else {
      buffer[count] = incomingByte;
      count++;
    }
  }
}
